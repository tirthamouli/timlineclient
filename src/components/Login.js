import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Input } from "react-materialize";
import { connect } from "react-redux";
import axios from "axios";
import { getPosts } from "../store/actions/getAction";

class Login extends Component {
  state = {
    submitDisabled: false,
    progressBar: "hidden",
    className: "hidden",
    email: "",
    password: ""
  };

  componentDidMount() {
    // redirecting if user is logged in
    if (localStorage.getItem("user") !== null) {
      this.props.history.push("/dashboard");
    } else {
      //Animation when component loads
      setTimeout(() => {
        this.setState({ className: "show" });
      }, 1);
    }
  }

  // Changing state when input is changed
  handleInput = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  // Login
  handleSubmit = e => {
    e.preventDefault();
    e.stopPropagation();

    this.setState({ progressBar: "show", submitDisabled: true });

    // creating post data
    let data = {
      email: this.state.email,
      password: this.state.password
    };

    axios.post("/user/login", data).then(response => {
      if ("error" in response.data) {
        window.M.toast({
          html: response.data.error.toUpperCase()
        });
        this.setState({ progressBar: "hidden", submitDisabled: false });
      } else {
        // Login was a success
        if ("id" in response.data && "token" in response.data) {
          window.localStorage.setItem("user", JSON.stringify(response.data));
          this.props.getPosts();
          this.props.history.push("/dashboard");
        }
      }
    });
  };

  render() {
    return (
      <div className={this.state.className}>
        <div className={this.state.progressBar + " progress"}>
          <div className="indeterminate" />
        </div>
        <center>
          <h1 className="grey-text lighten-4">Hello!</h1>
          <div className="section" />

          <h5 className="indigo-text">Please, login into your account</h5>
          <div className="section" />

          <div className="container">
            <div
              id="form-container"
              className=" signup-card-container z-depth-1 white lighten-4 row"
            >
              <form
                className="col s12"
                method="post"
                onSubmit={this.handleSubmit}
              >
                <div className="row">
                  <div className="col s12" />
                </div>

                <div className="row">
                  <div className="input-field col s12">
                    <Input
                      type="email"
                      name="email"
                      id="email"
                      s={12}
                      label="Email"
                      required
                      onChange={this.handleInput}
                      value={this.state.email}
                      icon="email"
                    />
                  </div>
                </div>

                <div className="row">
                  <div className="input-field col s12">
                    <Input
                      type="password"
                      name="password"
                      id="password"
                      s={12}
                      label="Password"
                      required
                      onChange={this.handleInput}
                      value={this.state.password}
                      icon="vpn_key"
                    />
                  </div>
                  <label style={{ float: "right" }}>
                    <a className="pink-text" href="#!">
                      <b>Forgot Password?</b>
                    </a>
                  </label>
                </div>

                <br />
                <center>
                  <div className="row">
                    <button
                      type="submit"
                      name="btn_login"
                      className="col s12 btn btn-large waves-effect pink indigo"
                      disabled={this.state.submitDisabled}
                    >
                      Login
                    </button>
                  </div>
                </center>
              </form>
            </div>
          </div>
          <Link to="/signup">Create account</Link>
        </center>

        <div className="section" />
        <div className="section" />
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  getPosts: props => {
    dispatch(getPosts(props));
  }
});

export default connect(
  null,
  mapDispatchToProps
)(Login);
