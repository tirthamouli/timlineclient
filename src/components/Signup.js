import React, { Component } from "react";
import { Input } from "react-materialize";
import { Link } from "react-router-dom";
import axios from "axios";

class Signup extends Component {
  state = {
    submitDisabled: false,
    progressBar: "hidden",
    className: "hidden",
    first_name: {
      value: "",
      valid: false
    },
    last_name: {
      value: "",
      valid: false
    },
    email: {
      value: "",
      valid: false
    },
    password: {
      value: "",
      valid: false
    }
  };

  componentDidMount() {
    // redirecting if user is logged in
    if (localStorage.getItem("user") !== null) {
      this.props.history.push("/dashboard");
    } else {
      // For initial animation
      setTimeout(() => {
        this.setState({ className: "show" });
      }, 1);
    }
  }

  // When something is input, then update the state accordingly
  handleInput = e => {
    let name = e.target.name;
    let value = e.target.value;
    this.setState(prevState => ({
      [name]: {
        ...prevState[name],
        value: value
      }
    }));
    this.handleValidation(e.target);
  };

  // Validate data when focus is lost
  handleValidation = elem => {
    let name = elem.name;
    let value = elem.value;
    let valid = false;
    if (name === "first_name" || name === "last_name") {
      if (value.match(/^[A-Za-z]+$/)) {
        valid = true;
      }
    } else if (name === "password") {
      if (
        value.match(
          // eslint-disable-next-line
          /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/
        )
      ) {
        valid = true;
      }
    } else if (name === "email") {
      if (
        value.match(
          // eslint-disable-next-line
          /^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/
        )
      ) {
        valid = true;
      }
    }

    this.setState(prevState => ({
      [name]: {
        ...prevState[name],
        valid: valid
      }
    }));
  };

  // handling form submit event
  handleSubmit = e => {
    e.preventDefault();
    e.stopPropagation();

    this.setState({ progressBar: "show", submitDisabled: true });

    if (
      this.state.first_name.valid !== true ||
      this.state.last_name.valid !== true
    ) {
      window.M.toast({ html: "NAME CAN ONLY HAVE ALPHABETS" });
      this.setState({ progressBar: "hidden", submitDisabled: false });
      return -1;
    } else if (this.state.email.valid !== true) {
      window.M.toast({ html: "INVALID EMAIL" });
      this.setState({ progressBar: "hidden", submitDisabled: false });
      return -1;
    } else if (this.state.password.valid !== true) {
      window.M.toast({
        html:
          "PASSWORD MUST HAVE A COMBINATION OF ALPHABETS, NUMBERS AND SPECIAL CHARS"
      });
      this.setState({ progressBar: "hidden", submitDisabled: false });
      return -1;
    }

    let data = {
      first_name: this.state.first_name.value,
      last_name: this.state.last_name.value,
      email: this.state.email.value,
      password: this.state.password.value
    };

    //Every field has been validated and hence can be sent
    axios.post("/user/signup", data).then(response => {
      if ("error" in response.data) {
        window.M.toast({
          html: response.data.error.toUpperCase()
        });
        this.setState({ progressBar: "hidden", submitDisabled: false });
      } else if ("success" in response.data) {
        // Registration is successfull and login user
        data = {
          email: this.state.email.value,
          password: this.state.password.value
        };

        axios.post("/user/login", data).then(response => {
          if ("error" in response.data) {
            window.M.toast({
              html: response.data.error.toUpperCase()
            });
            this.setState({ progressBar: "hidden", submitDisabled: false });
          } else {
            // Login was a success
            if ("id" in response.data && "token" in response.data) {
              window.localStorage.setItem(
                "user",
                JSON.stringify(response.data)
              );
              this.props.history.push("/dashboard");
            }
          }
        });
      }
    });
  };

  render() {
    return (
      <div className={this.state.className}>
        <div className={this.state.progressBar + " progress"}>
          <div className="indeterminate" />
        </div>
        <main>
          <center>
            <h1 className="center grey-text">Hi There!</h1>
            <div className="section" />

            <h5 className="indigo-text">Please, create a new account</h5>
            <div className="section" />

            <div className="container">
              <div
                className="z-depth-1 row signup-card-container"
                id="form-container"
              >
                <form className="col s12" onSubmit={this.handleSubmit}>
                  <div className="row">
                    <div className="input-field col s6">
                      <Input
                        type="text"
                        name="first_name"
                        id="first_name"
                        s={12}
                        label="First Name"
                        onChange={this.handleInput}
                        value={this.state.first_name.value}
                        required
                        icon="person"
                      />
                    </div>

                    <div className="input-field col s6">
                      <Input
                        type="text"
                        name="last_name"
                        id="last_name"
                        s={12}
                        label="Last Name"
                        onChange={this.handleInput}
                        value={this.state.last_name.value}
                        required
                        icon="perm_identity"
                      />
                    </div>
                  </div>
                  <div className="row">
                    <div className="input-field col s12">
                      <Input
                        type="email"
                        name="email"
                        id="email"
                        s={12}
                        label="Email"
                        onChange={this.handleInput}
                        value={this.state.email.value}
                        required
                        icon="email"
                      />
                    </div>
                  </div>
                  <div className="row">
                    <div className="input-field col s12">
                      <Input
                        type="password"
                        name="password"
                        id="password"
                        s={12}
                        label="Password"
                        onChange={this.handleInput}
                        value={this.state.password.value}
                        required
                        icon="vpn_key"
                      />
                    </div>
                  </div>
                  <div className="row">
                    <div className="row">
                      <div className="input-field col s12">
                        <button
                          type="submit"
                          name="btn_login"
                          className="col s12 btn btn-large waves-effect indigo"
                          disabled={this.state.submitDisabled}
                        >
                          Signup
                        </button>
                        <i className="mdi-content-send right" />
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            <Link to="/">Already have an account? Login</Link>
          </center>

          <div className="section" />
          <div className="section" />
        </main>
      </div>
    );
  }
}

export default Signup;
