import React from "react";

const PostSummary = props => {
  const { post } = props;
  return "image" in post ? (
    <div className="card post-summary">
      <div className="card-image">
        <img src={post.image} alt="" />
      </div>
      <div className="card-content">
        <span className="card-title">{post.description}</span>
        <p className="grey-text">{post.time}</p>
      </div>
    </div>
  ) : null;
};

export default PostSummary;
