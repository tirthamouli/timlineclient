import React, { Component } from "react";
import { connect } from "react-redux";
import { uploadImage, post } from "../store/actions/postAction";

class Write extends Component {
  state = {
    className: "hidden",
    description: ""
  };

  componentDidMount() {
    // redirecting if user is logged in
    if (localStorage.getItem("user") === null) {
      this.props.history.push("/");
    } else {
      //Animation when component loads
      setTimeout(() => {
        this.setState({ className: "show" });
      }, 1);
    }
  }

  //Hadling when file is attached
  handleFileAttach = e => {
    try {
      // Making form data
      var data = new FormData();
      data.append("file", e.target.files[0]);

      //Uploading image
      this.props.uploadImage(data, this.props);
    } catch (e) {
      console.log(e);
    }
  };

  //Handling when description is changed
  handleInput = e => {
    this.setState({
      description: e.target.value
    });
  };

  //When finally data is posted
  handleSubmit = e => {
    e.preventDefault();
    e.stopPropagation();
    const data = {
      description: this.state.description,
      postId: this.props.currentPost
    };
    this.props.post(data, this.props);
  };

  render() {
    const { currentPost, currentImage } = this.props;

    const central =
      currentPost === 0 ? (
        <button
          onClick={e => {
            this.inputElement.click();
          }}
          className="btn-floating waves-effect waves-dark btn-large grey lighten-3 attach-image button-icon"
          id="big-button"
        >
          <i className="material-icons">add</i>
          <form>
            <input
              ref={input => (this.inputElement = input)}
              type="file"
              name="attach"
              id="attach"
              onChange={this.handleFileAttach}
              autoComplete="off"
            />
          </form>
        </button>
      ) : (
        <div className="row">
          <img
            src={currentImage}
            alt="Error"
            className="col s12 m10 offset-m1"
          />
        </div>
      );
    return (
      <div className={this.state.className + " container"}>
        <h2 className="blue-text text-darken-2 center">Share your picture</h2>
        <h4 className="grey-text text-lighten-2 center">
          One picture is worth a thousand words
        </h4>
        <div className="center">
          {central}
          <form id="description-form" onSubmit={this.handleSubmit}>
            <div className="row">
              <div className="col s12 m6 offset-m3 input-field">
                <input
                  type="text"
                  name="description"
                  id="description"
                  onChange={this.handleInput}
                  value={this.state.description}
                  autoComplete="off"
                />
                <label htmlFor="description">Description</label>
              </div>
              <div className="col s12 m6 offset-m3 input-field">
                <button
                  type="submit"
                  className="btn btn-large waves-effect red lighten-2"
                  disabled={this.state.submitDisabled}
                >
                  Post
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  currentPost: state.currentPost,
  currentImage: state.currentImage
});

const mapDispatchToProps = dispatch => ({
  uploadImage: (data, props) => {
    dispatch(uploadImage(data, props));
  },
  post: (data, props) => {
    dispatch(post(data, props));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Write);
