import React, { Component } from "react";
import PostSummary from "./PostSummary";
import { connect } from "react-redux";

class Dashboard extends Component {
  state = {
    className: "hidden"
  };

  componentDidMount() {
    // redirecting if user is logged in
    if (localStorage.getItem("user") === null) {
      this.props.history.push("/");
    } else {
      //Animation when component loads
      setTimeout(() => {
        this.setState({ className: "show" });
      }, 1);
    }
  }
  render() {
    const { posts } = this.props;
    console.log(posts);
    const postList = posts.map(post => {
      return <PostSummary key={post.id} post={post} />;
    });

    return (
      <div className={this.state.className + " container"}>
        <div className="row add-story">
          <div className="col s12 m10 l8 offset-m1 offset-l2">
            <div className="card">
              <div className="card-content center">
                <span className="card-title">Add Story</span>
                <button
                  onClick={() => {
                    this.props.history.push("/write");
                  }}
                  className="btn-floating waves-effect waves-dark btn-large white center button-icon"
                >
                  <i className="material-icons">add</i>
                </button>
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col s12 m10 l8 offset-m1 offset-l2">{postList}</div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  posts: state.posts
});

export default connect(mapStateToProps)(Dashboard);
