import React, { Component } from "react";
import { Link, NavLink, withRouter } from "react-router-dom";
import "react-materialize";

class Navbar extends Component {
  //Logout user
  handleLogout = e => {
    e.preventDefault();
    e.stopPropagation();
    window.localStorage.removeItem("user");
    this.props.history.push("/");
  };

  componentDidMount() {
    var elems = document.querySelectorAll(".sidenav");
    window.M.Sidenav.init(elems);
  }

  componentDidUpdate() {
    var elems = document.querySelectorAll(".sidenav");
    window.M.Sidenav.init(elems);
  }

  render() {
    // Changing navbar based on which page the user is in
    const nav =
      window.localStorage.getItem("user") !== null ? (
        <nav className="nav-wrapper red lighten-2">
          <div className="container">
            <a href="/" data-target="mobile-demo" className="sidenav-trigger">
              <i className="material-icons">menu</i>
            </a>
            <Link to="/" className="brand-logo">
              TimeLine
            </Link>
            <ul className="right hide-on-med-and-down">
              <li>
                <NavLink to="/dashboard">
                  <strong>Dashboard</strong>
                </NavLink>
              </li>
              <li>
                <NavLink to="/write">
                  <strong>Write</strong>
                </NavLink>
              </li>
              <li>
                <a href="/" onClick={this.handleLogout}>
                  <strong>Logout</strong>
                </a>
              </li>
            </ul>
            <ul className="sidenav" id="mobile-demo">
              <li>
                <NavLink to="/dashboard">
                  <strong>Dashboard</strong>
                </NavLink>
              </li>
              <li>
                <NavLink to="/write">
                  <strong>Write</strong>
                </NavLink>
              </li>
              <li>
                <a href="/" onClick={this.handleLogout}>
                  <strong>Logout</strong>
                </a>
              </li>
            </ul>
          </div>
        </nav>
      ) : (
        <nav className="nav-wrapper red lighten-2">
          <a href="#" data-target="mobile-demo" className="sidenav-trigger">
            <i className="material-icons">menu</i>
          </a>
          <div className="container">
            <Link to="/" className="brand-logo">
              TimeLine
            </Link>
            <ul className="right hide-on-med-and-down">
              <li>
                <NavLink to="/">
                  <strong>Login</strong>
                </NavLink>
              </li>
              <li>
                <NavLink to="/signup">
                  <strong>Signup</strong>
                </NavLink>
              </li>
            </ul>
            <ul className="sidenav" id="mobile-demo">
              <li>
                <NavLink to="/">
                  <strong>Login</strong>
                </NavLink>
              </li>
              <li>
                <NavLink to="/signup">
                  <strong>Signup</strong>
                </NavLink>
              </li>
            </ul>
          </div>
        </nav>
      );
    return <div className="class-name">{nav}</div>;
  }
}

export default withRouter(Navbar);
