import React, { Component } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Login from "./components/Login";
import Signup from "./components/Signup";
import Navbar from "./components/Navbar";
import Dashboard from "./components/Dashboard";
import Write from "./components/Write";
import { connect } from "react-redux";
import { getPosts } from "./store/actions/getAction";

class App extends Component {
  componentDidMount() {
    this.props.getPosts(this.props);
  }
  render() {
    return (
      <BrowserRouter>
        <div className="App">
          <Navbar />
          <Switch>
            <Route path="/signup" component={Signup} />
            <Route path="/dashboard" component={Dashboard} />
            <Route path="/write" component={Write} />
            <Route path="/" component={Login} />
          </Switch>
        </div>
      </BrowserRouter>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  getPosts: props => {
    dispatch(getPosts(props));
  }
});

export default connect(
  null,
  mapDispatchToProps
)(App);
