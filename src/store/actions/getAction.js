import axios from "axios";

export const getPosts = props => {
  return (dispatch, getState) => {
    try {
      var user = JSON.parse(window.localStorage.getItem("user"));

      // Setting request header
      var headers = {
        authorization: JSON.stringify(user)
      };

      if (user === null) {
        return -1;
      }
      axios
        .get("/timeline/get", { headers: headers })
        .then(response => {
          dispatch({ type: "SET_POSTS", posts: response.data });

          // Getting image for each post
          response.data.forEach(post => {
            if (post.path !== null) {
              axios
                .get("/timeline/image/" + post.id, { headers: headers })
                .then(response => {
                  dispatch({
                    type: "SET_IMAGE_POST",
                    data: { image: response.data, id: post.id }
                  });
                });
            } else {
              dispatch({
                type: "SET_IMAGE_POST",
                data: { image: null, id: post.id }
              });
            }
          });
        })
        .catch(err => {
          // Something has been tampered with so logout user
          window.localStorage.removeItem("user");
          return props.history.push("/");
        });
    } catch (e) {
      // Something has been tampered with so logout user
      window.localStorage.removeItem("user");
      return props.history.push("/");
    }
  };
};
