import axios from "axios";

/*
 * Action for uploading an image
 * 
 */
export const uploadImage = (data, props) => {
  return (dispatch, getState) => {
    try {
      var user = JSON.parse(window.localStorage.getItem("user"));

      // Setting request header
      var headers = {
        authorization: JSON.stringify(user)
      };

      //Upload image request
      axios
        .post("/timeline/upload", data, { headers: headers })
        .then(response => {
          // Checking if response is correct
          if (
            typeof response === "object" &&
            "id" in response.data &&
            !isNaN(parseInt(response.data.id)) &&
            parseInt(response.data.id) !== 0
          ) {
            let id = parseInt(response.data.id);
            // Getting image data since upload was successful
            axios
              .get("/timeline/image/" + id, { headers: headers })
              .then(response => {
                dispatch({
                  type: "UPLOAD_SUCCESS",
                  id: id,
                  imageData: response.data
                });
              })
              .catch(err => {
                //Something has been tampered with
                window.localStorage.removeItem("user");
                return props.history.push("/");
              });
          } else {
            dispatch({ type: "UPLOAD_FAIL" });
          }
        })
        .catch(err => {
          //Something has been tampered with
          window.localStorage.removeItem("user");
          return props.history.push("/");
        });
    } catch (e) {
      // Something has been tampered with so logout user
      window.localStorage.removeItem("user");
      return props.history.push("/");
    }
  };
};

/*
 *Action for posting to timeline
 * 
 */
export const post = (data, props) => {
  return (dispatch, getState) => {
    try {
      var user = JSON.parse(window.localStorage.getItem("user"));

      // Setting request header
      var headers = {
        authorization: JSON.stringify(user)
      };

      //Posting
      axios
        .post("/timeline/post", data, { headers: headers })
        .then(response => {
          props.history.push("/dashboard");
          dispatch({ type: "POST_SUCCESS", data: response.data });
          // Getting image data
          let post = response.data;
          if (post.path !== null) {
            axios
              .get("/timeline/image/" + post.id, { headers: headers })
              .then(response => {
                dispatch({
                  type: "SET_IMAGE_POST",
                  data: { image: response.data, id: post.id }
                });
              });
          } else {
            dispatch({
              type: "SET_IMAGE_POST",
              data: { image: null, id: post.id }
            });
          }
        })
        .catch(err => {
          // Something has been tampered with so logout user
          window.localStorage.removeItem("user");
          props.history.push("/");
          dispatch({ type: "POST_FAIL" });
        });
    } catch (e) {
      // Something has been tampered with so logout user
      window.localStorage.removeItem("user");
      return props.history.push("/");
    }
  };
};
