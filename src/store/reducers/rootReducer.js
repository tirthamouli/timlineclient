const initialState = {
  currentPost: 0,
  currentImage: false,
  errorMessage: null,
  posts: []
};

const rootReducer = (state = initialState, action) => {
  switch (action.type) {
    case "UPLOAD_SUCCESS":
      return {
        ...state,
        currentPost: action.id,
        currentImage: action.imageData,
        errorMessage: null
      };
    case "UPLOAD_FAIL":
      return {
        ...state,
        errorMessage: "Upload failed"
      };
    case "POST_SUCCESS":
      return {
        ...state,
        currentPost: 0,
        currentImage: false,
        errorMessage: null,
        posts: [action.data, ...state.posts]
      };

    case "POST_FAIL":
      return {
        ...state,
        currentPost: 0,
        currentImage: false,
        errorMessage: "Post failed"
      };

    case "SET_POSTS":
      return {
        ...state,
        posts: action.posts
      };

    case "SET_IMAGE_POST":
      return {
        ...state,
        posts: state.posts.map(post => {
          if (post.id === action.data.id) {
            return { ...post, image: action.data.image };
          } else {
            return post;
          }
        })
      };

    default:
      return state;
  }
};

export default rootReducer;
